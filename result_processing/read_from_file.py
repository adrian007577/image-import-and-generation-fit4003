import csv

from Image_processing.transformation import Transformation
from result_processing.bounding_boxes import BoundingBox


def read_bounding_boxes(csvfile):
    reader = csv.DictReader(csvfile)
    boxes = dict()
    seed = None
    for line in reader:
        image_file = line['FILE_NAME']
        box = BoundingBox.read_from_csv(line)
        if image_file not in boxes:
            boxes[image_file] = []
        boxes[image_file].append(box)
        if box.tags:
            seed = image_file # assume only one seed file.
    return boxes, seed

def read_transformations(csvfile):
    reader = csv.DictReader(csvfile)
    boxes = dict()
    for line in reader:
        image_file = line['FILE_NAME']
        transformation = Transformation.from_csv(line)
        boxes[image_file] = transformation

    return boxes


if __name__ == "__main__":
    BOUNDING_BOX_FILE = "../results_raw/1571491496_Detailed.csv"

    with open(BOUNDING_BOX_FILE) as file:
        print(read_bounding_boxes(file))
