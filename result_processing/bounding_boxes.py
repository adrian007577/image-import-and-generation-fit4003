from Image_processing.transformation import Transformation
class BoundingBox:
    def __init__(self, class_type, probability, x1, y1, x2, y2):
        self.class_type = class_type
        self.probability = probability
        self.location = (x1, y1, x2, y2)
        self.tags = [] # Any additional information about this particular bounding box.

    def __str__(self):
        return f"{self.class_type}, {self.probability}, {self.location}"

    def __repr__(self):
        return str(self)

    def distance(self, b):
        """
        absolute distance between the corners of two rectangles
        :param b: another BoundingBox
        :return:
        """
        return sum(map(lambda x, y: abs(x - y), self.location, b.location))

    def apply_transformation(self, transformation):
        """
        Apply a transformation
        NOTE: scale, applied before offset
        scale is subject to precision point errors
        :param offset_x: offset originally applied
        :param offset_y: offset originally applied
        :param scale: scale originally applied as a float
        :return:
        """
        if not self.tags:
            return # no augmentation required
        x_min,y_min,x_max,y_max = self.location
        # flip y
        y_min = transformation.base_size[1] - y_min
        y_max = transformation.base_size[1] - y_max
        if 'FRONT_IMAGE' in self.tags:
            # apply scale
            x_min *= transformation.front_scale
            y_min *= transformation.front_scale
            x_max *= transformation.front_scale
            y_max *= transformation.front_scale
            # apply offset
            x_min += transformation.offset[0]
            y_min += transformation.offset[1]
            x_max += transformation.offset[0]
            y_max += transformation.offset[1]


        if any(tag in ['FRONT_IMAGE','BACK_IMAGE'] for tag in self.tags):
            # apply scale
            x_min *= transformation.scale
            y_min *= transformation.scale
            x_max *= transformation.scale
            y_max *= transformation.scale
            # apply base offset
            x_min += transformation.base_offset[0]
            y_min += transformation.base_offset[1]
            x_max += transformation.base_offset[0]
            y_max += transformation.base_offset[1]

        # unflip y
        y_min = transformation.base_size[1] - y_min
        y_max = transformation.base_size[1] - y_max

        # Constrain the maximum to be the maximum of the image
        if x_min < 0:
            x_min = 0
        if y_min < 0:
            y_min = 0
        if x_max > transformation.base_size[0]:
            x_max = transformation.base_size[0]
        if y_max > transformation.base_size[1]:
            y_max = transformation.base_size[1]


        self.location = (int(x_min), int(y_min), int(x_max), int(y_max))

    def reverse_transformation(self,transformation):
        """
        Reverse a transformation
        :param transformation: a Transformation to be reversed
        :return:
        """
        if not self.tags:
            return # no augmentation required
        x_min,y_min,x_max,y_max = self.location
        # flip y
        y_min = transformation.base_size[1] - y_min
        y_max = transformation.base_size[1] - y_max
        if any(tag in ['FRONT_IMAGE','BACK_IMAGE'] for tag in self.tags):
            # remove base offset
            x_min -= transformation.base_offset[0]
            y_min -= transformation.base_offset[1]
            x_max -= transformation.base_offset[0]
            y_max -= transformation.base_offset[1]
            # remove scale
            x_min /= transformation.scale
            y_min /= transformation.scale
            x_max /= transformation.scale
            y_max /= transformation.scale

        if 'FRONT_IMAGE' in self.tags:
            # remove offset
            x_min -= transformation.offset[0]
            y_min -= transformation.offset[1]
            x_max -= transformation.offset[0]
            y_max -= transformation.offset[1]
            # remove scale
            x_min /= transformation.front_scale
            y_min /= transformation.front_scale
            x_max /= transformation.front_scale
            y_max /= transformation.front_scale

        # unflip y
        y_min = transformation.base_size[1] - y_min
        y_max = transformation.base_size[1] - y_max
        self.location = (int(x_min),int(y_min),int(x_max),int(y_max))

    @staticmethod
    def read_from_csv(csv_dictionary):
        box = BoundingBox(csv_dictionary['OBJECT'], float(csv_dictionary['PROBABILITY']),
                          int(csv_dictionary['MIN_X']),int(csv_dictionary['MIN_Y']),
                          int(csv_dictionary['MAX_X']), int(csv_dictionary['MAX_Y']))
        img_type = csv_dictionary['IMAGE_TYPE']
        if img_type:
            box.tags.append(img_type)
        return box
