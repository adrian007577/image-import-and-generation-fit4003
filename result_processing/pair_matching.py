"""

Robin Phoeng 18/10/19
Process results

This is made to work against a csv file of format

filename, class, probability, min x, min y, max x, max y.

"""
import copy
import csv

from Image_processing.transformation import Transformation
from result_processing.bounding_boxes import BoundingBox
from result_processing.read_from_file import read_bounding_boxes, read_transformations


def calculate_pairs(expectation, actual, threshold):
    """
    Group bounding boxes according to their closest pair
    A Naive approach is currently used (unoptimized time complexity)
    A pair is considered only if the closest box of A is B and B is A
    This can be configured with a threshold, where anything larger than threshold is not considered

    :param a: array of BoundingBox to compare
    :param b: array of BoundingBox to compare
    :return: list of ordered pairs (a,b)
    """
    pairs_e = dict()
    for box_e in expectation:
        # Default to threshold
        min_box = None
        min_distance = threshold
        for box_a in actual:
            if box_a.class_type != box_e.class_type:
                continue
            new_distance = box_a.distance(box_e)
            if new_distance < min_distance:
                min_box = box_a
                min_distance = new_distance
        pairs_e[box_e] = min_box


    # calculate closest rectangle for b
    pairs_a = dict()
    for box_a in actual:
        # Default to threshold
        min_box = None
        min_distance = threshold
        for box_e in expectation:
            if box_e.class_type != box_a.class_type:
                continue
            new_distance = box_e.distance(box_a)
            if new_distance < min_distance:
                min_box = box_e
                min_distance = new_distance
        pairs_a[box_a] = min_box

    # check if pairs match, if they don't, remove pairing
    pairs = []
    for box_e in expectation:
        box_a = pairs_e[box_e]
        if box_a is not None and pairs_a[box_a] == box_e:
            pairs.append((box_e, box_a))
        else:
            pairs.append((box_e, None))

    # If a pair exists, it would already be added from above, so only add not matching
    for box_a in actual:
        box_e = pairs_a[box_a]
        if box_e is None or pairs_e[box_e] != box_a:
            pairs.append((None, box_a))

    return pairs


def compare(seed, subjects, transformations, threshold):
    """
    Compare a seed image bounding box to another
    :param seed: The original
    :param subjects: dictionary of box sets to compare to
    :param transformations : transformations the subject was put under
    :param threshold : pixel accuracy when pairing objects
    :return: object detected, with a difference in probability, positive being greater confidence than original.
    """
    # first things first, remove the transformation applied
    base_transform = transformations[seed_file]
    for box in seed:
        box.reverse_transformation(base_transform)

    comparisons = dict()

    for key in subjects:
        subject = subjects[key]

        if key not in transformations:
            print(key)
            raise Exception("No transformation for transform")
        transformation = transformations[key]

        # generate the expected morph.
        expectation = copy.deepcopy(seed)
        for box in expectation:
            box.apply_transformation(transformation)

        pairs = calculate_pairs(expectation, subject, threshold)
        comparisons[key] = calculate_metrics(pairs,transformation,base_transform,len(seed))

    return comparisons


def calculate_metrics(pairs,transformation, original_transform, total_expected):
    """
    Generate a table of results, based purely on obsfucation
    :param pairs: a set of tuples running (seed,subject)
    :param transformation: the transformation applied
    :param original_transform: the transformation of the seed pair
    :param total_expected: the number of expected match elements
    :return: tuple, of object count, total_confidence different (no matches ignored), overlap difference.
    positive overlap difference means the second pair is more covered up.
    """
    total_diff = 0
    object_count = 0
    overlap_diff = transformation.overlap - original_transform.overlap
    for pair in pairs:
        if pair[0] is None or pair[1] is None:
            continue
        confidence_diff = pair[1].probability - pair[0].probability
        object_count += 1
        total_diff += confidence_diff

    return object_count,total_diff, overlap_diff, total_expected

if __name__ == "__main__":
    OUTPUT_FILE_DET = csv.writer(open('../results_processed/output.csv', 'w', newline=''), delimiter=',')
    INPUT_BOUND_BOX = 'car_back_white'
    INPUT_TRANSFORM = 'car_back_white_transformations'
    with open(f"../results_raw/{INPUT_BOUND_BOX}.csv") as file_b, open(f"../results_raw/{INPUT_TRANSFORM}.csv") as file_t:
        boxes, seed_file = read_bounding_boxes(file_b)
        transformations = read_transformations(file_t)
        seed = boxes[seed_file]
        del boxes[seed_file] # remove seed from transform, so it doesn't compare against itself
        OUTPUT_FILE_DET.writerow(['File','Objects Detected', 'Average Confidence Difference', 'Additional Overlap percentage', 'Averaged Overall Confidence Difference'])
        comparisons = compare(seed, boxes, transformations, 1000)
        for i in sorted(comparisons.items(),key = lambda a : a[1][2]):
            key = i[0]
            object_count,total_diff, overlap_diff, total_expected = i[1]
            average = 0 if object_count == 0 else total_diff/object_count
            expected_average = total_diff / total_expected
            OUTPUT_FILE_DET.writerow([key,object_count,average,overlap_diff,expected_average])
