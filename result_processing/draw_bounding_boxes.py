"""
Robin Phoeng 20/10/19

From a source image, and set of bounding boxes, draw on the image a rectangle with label


"""
import os

from PIL import ImageDraw, Image, ImageFont

from result_processing.bounding_boxes import BoundingBox
from result_processing.read_from_file import read_bounding_boxes

IMAGE_INPUT_DIRECTORY = '../image_processed'
IMAGE_BOX_DIRECTORY = '../results_raw'
IMAGE_SAVE_DIRECTORY = '../results_image_processed'

def draw_bounding_box(image, boxes):
    """
    Returns a copy with images pasted on top of the boxes
    :param image: image to paste boxes on top of
    :param boxes: array of BoundingBoxes to draw
    :return:
    """
    text_size = 40
    canvas = image.copy()
    draw = ImageDraw.Draw(canvas)
    font = ImageFont.truetype("arial.ttf",size=text_size)
    for box in boxes:

        text_location = (box.location[0], box.location[1]-text_size)

        text_size_length = draw.textsize(box.class_type,font)

        text_wrap_location = (box.location[0],box.location[1]-text_size_length[1],box.location[0] + text_size_length[0],box.location[1])
        draw.rectangle(text_wrap_location, fill=(0, 0, 0, 10), width=10)
        draw.text(text_location,box.class_type,font=font,fill=(255,255,255,10))

        draw.rectangle(box.location,outline=(0,0,0,255),width=10)

    return canvas

def draw_bounding_boxes_all(image_directory,box_file,prefix=''):
    """
    Creates bounding box images against all files in the image directory
    :param image_directory: directory name of image files
    :param box_file: a file that contains the bounding boxes of all files
    :return:
    """

    bounding_boxes, seed = read_bounding_boxes(box_file)

    for filename in os.listdir(image_directory):
        if filename.startswith(prefix):
            sequence = filename[len(prefix):]
            if sequence in bounding_boxes:
                image = Image.open(f"{image_directory}/{filename}")
                processed_image = draw_bounding_box(image,bounding_boxes[sequence])
                processed_image.save(f"{IMAGE_SAVE_DIRECTORY}/{filename}")


if __name__ == "__main__":
    # JUST one image
    """
    human = Image.open(f"{IMAGE_INPUT_DIRECTORY}/0_0_o0.03236916218819967.png")
    with open(f"{IMAGE_BOX_DIRECTORY}/seed.csv") as file:
        bounding_box_process = read_file(file)
        bounding_boxes = bounding_box_process['0_0_o0.03236916218819967.png']
        processed = draw_bounding_box(human,bounding_boxes)
        processed.show()
    """
    with open(f"{IMAGE_BOX_DIRECTORY}/car_back_white.csv") as file:

        draw_bounding_boxes_all(image_directory=IMAGE_INPUT_DIRECTORY,box_file=file,prefix='')