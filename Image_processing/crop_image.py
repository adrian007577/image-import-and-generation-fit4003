from PIL import Image, ImageChops

if __name__ == "__main__":
    IMAGE = '../image_raw/car_back_white.png'
    cropping = Image.open(IMAGE)
    cropped = ImageChops.offset(cropping, 100, 0)
    cropped = cropped.crop((0,81,1360,748))
    cropped.save('../image_raw/processed.png')