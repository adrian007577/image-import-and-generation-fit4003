class Transformation:
    csv_headers = ['OFFSET_X', 'OFFSET_Y', 'SCALE_FRONT', 'SCALE', 'OBFUSCATION', 'BASE_OFFSET_X', 'BASE_OFFSET_Y',
                   'BACKGROUND_WIDTH','BACKGROUND_HEIGHT']

    def __init__(self, offset=(0, 0), front_scale=1.0, scale=1.0, base_offset=(0, 0), base_size=(0, 0), overlap=None):
        self.offset = offset
        self.front_scale = front_scale # scaling of the front image
        self.scale = scale
        self.overlap = overlap
        self.base_offset = base_offset
        self.base_size = base_size

    def __str__(self):
        return f"{self.offset[0]}, {self.offset[1]}, {self.front_scale}, {self.scale}, {self.overlap}"

    def __repr__(self):
        return str(self)

    def to_csv(self):
        return [self.offset[0], self.offset[1], self.front_scale,
                self.scale, self.overlap, self.base_offset[0], self.base_offset[1], self.base_size[0], self.base_size[1]]

    @staticmethod
    def from_csv(csv_dictionary):
        transformation = Transformation((int(csv_dictionary['OFFSET_X']), int(csv_dictionary['OFFSET_Y'])),
                                        float(csv_dictionary['SCALE_FRONT']), float(csv_dictionary['SCALE']),
                                        (int(csv_dictionary['BASE_OFFSET_X']), int(csv_dictionary['BASE_OFFSET_Y'])),
                                        (int(csv_dictionary['BACKGROUND_WIDTH']), int(csv_dictionary['BACKGROUND_HEIGHT'])),
                                        float(csv_dictionary['OBFUSCATION']))
        return transformation
