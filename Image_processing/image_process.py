import os
"""
TODO combine this mapping to create a set of images. possibly even load this into a csv instead so that it can be
done offline
"""

def create_mapping(files):
    mapping = dict()
    mapping['human'] = []
    mapping['background'] = []
    mapping['object'] = []
    for entry in files:
        if 'human' in entry.name:
            mapping['human'].append(entry)
        elif 'background' in entry.name:
            mapping['background'].append(entry)
        else:
            mapping['object'].append(entry)

    return mapping

IMAGE_INPUT_DIRECTORY = '../image_raw'
IMAGE_OUTPUT_DIRECTORY = '../image_processed'

if __name__ == "__main__":

    with os.scandir(IMAGE_INPUT_DIRECTORY) as files:
        mapping = create_mapping(files)

        print(mapping)