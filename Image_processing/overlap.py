"""
Robin Phoeng 15/10/19

Overlay one image over another image, with a metric generated
Current Metrics:
Overlay% - how much the front image covers the back image.

Assumptions made:
Images start normally, and the 'back' image will be moved around the front image.
Images start at the correct height/ width ratios.
Images have channels of alpha, which is a binary.
"""
from PIL import Image
import numpy as np


def calculate_overlap(front_image, back_image, offset=(0, 0)):
    """
    Calculate a percentage of overlap of a front image and back image, along with an additional offset
    :param front_image: a front image mask
    :param back_image: a back image mask
    :param offset: offset of the front image.
    :return: a percentage between 0 and 1 of overlap.
    """
    # arrays are [height][width] from the top left corner
    front_array = np.array(front_image)
    back_array = np.array(back_image)

    # calculate the intersection bounding box (the only place needed to check overlap.)
    back_bbox = back_image.getbbox()
    front_bbox = front_image.getbbox()
    intersect_bbox = (max(back_bbox[0], front_bbox[0] + offset[0]),
                      max(back_bbox[1], front_bbox[1] + offset[1]),
                      min(back_bbox[2], front_bbox[2] + offset[0]),
                      min(back_bbox[3], front_bbox[3] + offset[1]))

    # calculate number of pixels in the original image.
    # ONLY LOOKING AT ALPHA CHANNEL.
    back_count = np.count_nonzero(back_array.compress([False,False,False,True],axis=2))

    # get the part that is actually overlapping of front image.
    f_min_x = intersect_bbox[0] - offset[0] if back_bbox[0] >= front_bbox[0] + offset[0] else front_bbox[0]
    f_min_y = intersect_bbox[1] - offset[1] if back_bbox[1] >= front_bbox[1] + offset[1] else front_bbox[1]
    f_max_x = intersect_bbox[2] - offset[0] if back_bbox[2] <= front_bbox[2] + offset[0] else front_bbox[2]
    f_max_y = intersect_bbox[3] - offset[1] if back_bbox[3] <= front_bbox[3] + offset[1] else front_bbox[3]

    back_intersection = back_array[intersect_bbox[1]:intersect_bbox[3], intersect_bbox[0]:intersect_bbox[2]]
    front_intersection = front_array[f_min_y:f_max_y, f_min_x:f_max_x]

    back_pixels = np.any(back_intersection.compress([False,False,False,True],axis=2) != 0, axis=2)
    front_pixels = np.any(front_intersection.compress([False,False,False,True],axis=2) != 0, axis=2)
    overlap = np.logical_and(back_pixels, front_pixels)
    intersect_count = np.count_nonzero(overlap)

    return intersect_count / back_count


if __name__ == "__main__":
    IMAGE_INPUT_DIRECTORY = '../image_raw'
    IMAGE_OUTPUT_DIRECTORY = '../image_cooked'
    # Load images
    human = Image.open(f"{IMAGE_INPUT_DIRECTORY}/human.png")
    lightpole = Image.open(f"{IMAGE_INPUT_DIRECTORY}/car_back_white.png")
    offset = (-280, 0)

    flipped_human = human.transpose(Image.FLIP_TOP_BOTTOM)
    flipped_lightpole = lightpole.transpose(Image.FLIP_TOP_BOTTOM)

    merged = flipped_human.copy()
    merged.paste(flipped_lightpole, offset, flipped_lightpole)
    merged = merged.transpose(Image.FLIP_TOP_BOTTOM)
    merged.save(f"{IMAGE_OUTPUT_DIRECTORY}/overlap.png")

    percentage = calculate_overlap(flipped_lightpole, flipped_human, offset)
    print(f"percentage is : {100 * percentage}")
