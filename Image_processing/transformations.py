"""
Robin Phoeng 16/10/19

Move an image around another image.

"""
import csv

from PIL import Image

from Image_processing.overlap import calculate_overlap
from Image_processing.transformation import Transformation

IMAGE_INPUT_DIRECTORY = '../image_raw'
IMAGE_OUTPUT_DIRECTORY = '../image_processed'
OUTPUT_BOUND_DIR = '../image_processed'

OUTPUT_FILE_DET = None
def image_transformation(transformations,base_plate,back_image,front_image):
    """
    Move a front image around a background image
    NOTE: front image scaling is done first, then offset is applied, finally total scale is done at the end.
    Images are also handled from the bottom left corner
    :param transformations: a (x,y,ratio) tuple list of transformations
    :param base_plate: where the two images will be put onto.
    :param back_image: the back image to cover over
    :param front_image: the front image to write over.
    :return:
    """
    # Flip images so we can can consider them from the bottom left corner (which is transposed to the top left
    flipped_base = base_plate.transpose(Image.FLIP_TOP_BOTTOM)
    flipped_back = back_image.transpose(Image.FLIP_TOP_BOTTOM)
    flipped_front = front_image.transpose(Image.FLIP_TOP_BOTTOM)

    for transformation in transformations:
        resized_front = resize_image(flipped_front, transformation.front_scale)
        overlap = calculate_overlap(resized_front, flipped_back,transformation.offset)

        # TODO create a canvas so the total transformation and base scale is applied afterwards.
        # create a blank canvas big enough for the images.
        resized_back = resize_image(flipped_back, transformation.scale)
        resized_front = resize_image(resized_front, transformation.scale)
        transformation.overlap = overlap

        # apply background image
        merged = flipped_base.copy()
        front_offset = (int(transformation.scale * transformation.offset[0]) + transformation.base_offset[0],
                        int(transformation.scale * transformation.offset[1]) + transformation.base_offset[1])
        merged.paste(resized_back,transformation.base_offset,resized_back)
        merged.paste(resized_front, front_offset,resized_front)
        # Unflip the image so it can be saved.
        merged = merged.transpose(Image.FLIP_TOP_BOTTOM)
        transformation.base_size = merged.size # record background information
        yield transformation, merged

def resize_image(image,scale):
    # returns a copy of the image.
    return image.resize((int(image.width * scale), int(image.height * scale)))

def place_image(back_image,front_image, offset = None):
    """
    Places an image at the bottom left (modifies existing image)
    :param back_image:
    :param front_image:
    :param offset:
    :return:
    """
    if offset == None:
        offset = (0,0)
    back_image.paste(front_image,(offset[0],back_image.height-front_image.height - offset[1]),front_image)

def create_transformations():
    """
    Generate a set of transformations for offsets and scale of front image and overall scale (simulating distance)
    :return:
    """
    # scale as a percentage
    for total_scale in range(40, 101, 20): # 3 # go in smaller gradients, but more localized area.
        for front_scale in range(150,200,5): # 7
            for x in range(-600, 201, 20): # 20
                yield Transformation((x,0),front_scale/100,total_scale/100,(400,0))

if __name__ == "__main__":
    # Initialize csv file
    OUTPUT_FILE_DET = csv.writer(open(OUTPUT_BOUND_DIR + '/output.csv', 'w',newline=''), delimiter=',')
    headers = ['FILE_NAME', 'BACK_IMAGE', 'FRONT_IMAGE']
    headers.extend(Transformation.csv_headers)
    OUTPUT_FILE_DET.writerow(headers)

    front_image_name = 'car_back_white.png'
    human = Image.open(f"{IMAGE_INPUT_DIRECTORY}/human.png")
    lightpole = Image.open(f"{IMAGE_INPUT_DIRECTORY}/{front_image_name}")
    background = Image.open(f"{IMAGE_INPUT_DIRECTORY}/background_clean.jpg")

    sequence = 0
    for transformation, merged in image_transformation(create_transformations(),background,human,lightpole):
        # write this shit out into a file?? with a sequence number.
        name = f"{sequence}.png"
        row = [name,'human.png',front_image_name]
        row.extend(transformation.to_csv())
        OUTPUT_FILE_DET.writerow(row)
        merged.save(f"{IMAGE_OUTPUT_DIRECTORY}/{name}")
        sequence += 1





