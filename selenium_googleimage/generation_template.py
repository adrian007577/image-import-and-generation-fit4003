from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import urllib.parse
import requests
import os
import random
import string


def random_string(string_length ):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


keyword = input("Please Enter Something to search  ")

base_url = "https://www.google.com/search?tbm=isch&q="

search_url = base_url+keyword

print(search_url)

driver = webdriver.Firefox()

driver.get(search_url)

all_img = driver.find_elements_by_class_name("rg_l")

path = "C:/Users/adria/PycharmProjects/image-import-and-generation-fit4003/selenium_googleimage/Image_raw/"

for img in all_img:
    img =  img.get_attribute("href")
    link = ""
    read = False
    for letter in img:
        if letter == '&' and read == True:
            break
        if read:
            link += letter
        if letter == '=':
            read = True

    if len(link) > 10:
        link = urllib.parse.unquote(link)

        print(link)

        temp_path = path + random_string(10) + ".jpg"
        print(temp_path)

        r = requests.get(link, stream=True)
        if r.status_code == 200:
            with open(temp_path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)


driver.close()

# https://www.google.com/search?tbm=isch&q=hi